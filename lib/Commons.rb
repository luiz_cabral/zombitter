require 'digest/sha1'

module Commons
  class Encripter

    def hashify(params)
      Digest::SHA1.hexdigest(params)
    end
  end
end
