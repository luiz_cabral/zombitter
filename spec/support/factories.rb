# This will guess the User class
FactoryBot.define do
  factory :user do
    trait :valid_login do
      login "test"
    end
    trait :valid_pass do
      password "pass"
    end

    trait :edge_login do
      login "tes"
    end

    trait :edge_pass do
      password "pas"
    end

    trait :bad_login do
      login "te"
    end

    trait :bad_pass do
      password "pa"
    end

    trait :complete_user do
      login "test"
      password "pass"
    end

  end

  factory :post do
    trait :valid_user do
      association :user, factory: [:user, :valid_login, :valid_pass], strategy: :build
    end

    trait :valid_body do
      body "a"
    end

    trait :empty_body do
      body ""
    end
  end
end
