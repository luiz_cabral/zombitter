require 'rails_helper'

RSpec.describe Post, type: :model do
  describe 'validation' do

    context 'with user and not empty body present' do
      it 'is valid' do
        post = build(:post, :valid_user, :valid_body)
        p post

        expect(post).to be_valid
      end
    end

    context 'with user and empty body present' do
      it 'is not valid' do
        post = build(:post, :valid_user, :empty_body)

        expect(post).not_to be_valid
      end
    end

    context 'with user and no body' do
      it 'is not valid' do
        post = build(:post, :valid_user)

        expect(post).not_to be_valid
      end
    end

    context 'with not empty body and no user' do
      it 'is not valid' do
        post = build(:post, :valid_body)

        expect(post).not_to be_valid
      end
    end

  end

  describe '.save' do

    context 'with valid post' do
      it 'persists model to db' do
        valid_post = build(:post, :valid_user, :valid_body)

        valid_post.save

        expect(valid_post).to be_persisted
      end
    end

    context 'with invalid post' do
      it 'does not persist model' do
        invalid_post = build(:post)

        invalid_post.save

        expect(invalid_post).not_to be_persisted
      end
    end

  end

  describe 'Associations' do
    context 'with user' do
      it 'belongs to users' do
        assc = described_class.reflect_on_association(:user)
        expect(assc.macro).to eq :belongs_to
      end
    end
  end

end
