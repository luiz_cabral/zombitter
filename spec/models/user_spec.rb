require 'rails_helper'

RSpec.describe User, type: :model do
  describe 'validation' do
    context 'When parameters present and larger than 3 chars' do
      it 'is valid' do
        user = build(:user, :valid_login, :valid_pass)

        expect(user).to be_valid
      end
    end

    context 'When parameters present and size equal to 3' do
      it 'is valid if login size' do
        user = build(:user, :edge_login, :valid_pass)

        expect(user).to be_valid
      end

      it 'is valid if password size' do
        user = build(:user, :valid_login, :edge_pass)

        expect(user).to be_valid
      end

      it 'is valid if both size' do
        user = build(:user, :edge_login, :edge_pass)

        expect(user).to be_valid
      end
    end

    context 'When parameters present and size lesser tha 3' do
      it 'is valid if login size' do
        user = build(:user, :bad_login, :valid_pass)

        expect(user).not_to be_valid
      end

      it 'is valid if password size' do
        user = build(:user, :valid_login, :bad_pass)

        expect(user).not_to be_valid
      end

      it 'is valid if both size' do
        user = build(:user, :bad_login, :bad_pass)

        expect(user).not_to be_valid
      end
    end

    context 'When one parameter not present and other larger than 3' do
      it 'is not valid if password not present' do
        user = build(:user, :valid_login)

        expect(user).not_to be_valid
      end
      it 'is not valid if login not present' do
        user = build(:user, :valid_pass)

        expect(user).not_to be_valid
      end
    end

    context 'When no parameters present' do
      it 'is not valid' do
        user = build(:user)

        expect(user).not_to be_valid
      end
    end

  end

  describe 'save' do

    context 'When valid_user and password_plaintext present' do
      it 'persists model to db' do
        valid_user = build(:user, :valid_login, :valid_pass)
        valid_user.password_plaintext = "pass"

        valid_user.save

        expect(valid_user).to be_persisted
      end
    end

    context 'when valid_user and no password_plaintext' do
      it 'does not persist model to db' do
        valid_user = build(:user, :valid_login, :valid_pass)

        valid_user.save

        expect(valid_user).not_to be_persisted
      end
    end

    context 'when invalid_user' do
      it 'does not persist model to db' do
        invalid_user = build(:user)

        invalid_user.save

        expect(invalid_user).not_to be_persisted
      end
    end

  end

  describe 'Associations' do
    context 'with post' do
      it 'has many posts' do
        assc = described_class.reflect_on_association(:posts)
        expect(assc.macro).to eq :has_many
      end
    end
  end

end
