Rails.application.routes.draw do
  get 'login/index'
  post 'login/signin'
  get 'login/signup'
  delete 'login/signout'

  resources :users, only: [:new, :create] do
    resources :posts

    get 'all_posts', to: 'posts#all'
  end


  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root 'login#index'
end
