class ChangeUserLoginUniqueness < ActiveRecord::Migration[5.1]
  def change
    reversible do |dir|
      dir.up do
        change_column :users, :login, :string, unique: true
      end

      dir.down do
        change_column :users, :login, :string, unique: false
      end
    end
  end
end
