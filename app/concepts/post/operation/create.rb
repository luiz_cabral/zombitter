require "trailblazer"

#:createop
class Post::Create < Trailblazer::Operation
  class Present < Trailblazer::Operation
    step Model(Post, :new)
    step Contract::Build( constant: Post::Contract::Create )
  end

  step Nested( Present )
  step Contract::Validate(key: 'new_post')
  step Contract::Persist()
  #~present end
end
#:createop end
