require "trailblazer"

#:createop
class Post::Delete < Trailblazer::Operation
  step :model!
  step :delete!

  def model!(options, params:, **)
    options["model"] = Post.find_by_id(params[:id]) # might return false!
  end

  def delete!(options, model:, **)
    model.destroy
  end
  #~present end
end
#:createop end
