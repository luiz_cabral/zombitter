require "trailblazer"

#:createop
class Post::All < Trailblazer::Operation
  step :get_all!

  def get_all!(options, **)
    options["model"] = ::Post.all.reverse_order
  end
  #~present end
end
#:createop end
