#:contract
require "reform"
require "reform/form/dry"

module Post::Contract
  class Create < Reform::Form
    include Dry

    #:property
    property :body
    property :user_id
    property :created_at
    property :updated_at
    #:property end

    #:validation
    validation do
      required(:body).filled(min_size?: 1)
    end
    #:validation end
  end
end
#:contract end
