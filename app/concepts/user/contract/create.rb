#:contract
require "reform"
require "reform/form/dry"

module User::Contract
  class Create < Reform::Form
    include Dry

    #:property
    property :login
    property :password
    #:property end

    #:validation
    validation do
      required(:login).filled(min_size?: 3)
      required(:password).filled(min_size?: 3)
    end
    #:validation end
  end
end
#:contract end
