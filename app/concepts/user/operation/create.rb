require "trailblazer"
require_relative "../contract/create"

#:createop
class User::Create < Trailblazer::Operation
  class Present < Trailblazer::Operation
    step Model(User, :new)
    step Contract::Build( constant: User::Contract::Create )
  end

  #~present
  step Nested( Present )
  step :double_check_password!
  step :password_plaintext!
  step Contract::Validate( key: :user )
  step Contract::Persist( )

  def double_check_password!(options, params:, model:,  **)
    params["user"]["password"] != nil && params["user"]["password"] == params["user"]["confirm_password"]
  end

  def password_plaintext!(options, params:, model:, **)
    model.password_plaintext = params["user"]["confirm_password"]
  end
  #~present end
end
#:createop end
