require "trailblazer"

#:createop
class User::Login < Trailblazer::Operation
  step -> (options, params:, **){ options["user"] = params["user"] }
  step :model!
  step :check!
  failure :clear_all!

  def model!(options, params:, user:, **)
    options["model"] = User.where(user.permit(:login)).first # might return false!
  end

  def check!(options, params:, model:, user:, **)
    model["password"] == Commons::Encripter.new.hashify(user["password"])
  end

  def clear_all!(options, params:, model:, user:, **)
    model = User.new()
  end

  #~present end
end
#:createop end
