require "trailblazer"

#:createop
class User::Authorize < Trailblazer::Operation
  step :authorize!

  def authorize!(options, user_id:, **)
    options["user"] = User.find_by_id(user_id["id"])
  end

  #~present end
end
#:createop end
