require "#{Rails.root}/lib/commons"
class User < ApplicationRecord
  has_many :posts, dependent: :destroy
  validates :login, uniqueness: true
  validates :login, :password, presence: true
  validates :password, :login, length: { minimum: 3 }

  attr_accessor :password_plaintext
  before_save :hashfy

  private
  def hashfy
    if password_plaintext.present?
      self.password = Commons::Encripter.new.hashify(password_plaintext)
    else
      throw :abort
    end
  end
end
