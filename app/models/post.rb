class Post < ApplicationRecord
  belongs_to :user

  validates :body, presence: true
  validates :body, length: { minimum: 1 }
end
