require "#{Rails.root}/lib/commons"
class LoginController < ApplicationController
  def index
  end

  def signup
  end

  def signin
    run User::Login do |result|
      session[:current_user_id] = result["model"].id
      return redirect_to user_posts_path(result["model"])
    end

    flash.now[:error] = "Wrong credentials"
    render action: 'index'
  end

  def signout
    session.delete(:current_user_id)
    render 'index'
  end
end
