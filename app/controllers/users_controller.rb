class UsersController < ApplicationController

  def new
    run User::Create::Present do |result|
      @user = result.model
    end
  end

  def create
    run User::Create do |result|
      session[:current_user_id] = result["model"].id
      return redirect_to user_posts_path(result["model"])
    end

    redirect_to login_signup_path, flash: { error:  "Passwords don't match" }
  end

end
