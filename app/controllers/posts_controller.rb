class PostsController < ApplicationController
  before_action :authenticate
  layout "navbar"

  def index
  end

  def create
    Post::Create.(params)
    redirect_to user_posts_path(@user)
  end

  def destroy
    Post::Delete.(params)
    redirect_to user_posts_path(@user)
  end

  def all
    result = Post::All.()
    @posts = result["model"]
  end
end
