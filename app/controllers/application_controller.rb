class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  def authenticate
    if result = User::Authorize.(params, :user_id => { "id" => session[:current_user_id] })
      return @user = result["user"]
    end

    redirect_to login_index_path, flash: { error:  "You must be logged in to access this section" }
  end
end
